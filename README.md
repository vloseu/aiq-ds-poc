# AIQ-Stencil-POC

This project is a monorepo for AIQ Design System (AIQ-DS) UI library.
The monorepo setup is based on [Lerna](https://lerna.js.org) and Yarn Workspaces.

## Prerequisites

Make sure you have [Yarn](https://yarnpkg.com) installed.

This POC expects that you have a locally running private npm registry.

To run local instance of Verdaccio npm registry with Docker:
- `docker run -it --rm --name verdaccio -p 4873:4873 verdaccio/verdaccio`

Alternatively, you can install and run Verdaccio as global npm package:
- `npm i -g verdaccio`
- `verdaccio`

Next add npm user to your local registry (if you haven't modified Verdaccio port, it should be 4873):
- `npm adduser --registry http://localhost:4873`
- `npm login` (use username and password from previous step)

## How to start

Install dependencies for all packages, run the following command:
- `yarn bootstrap`

Build and publish first version of UI libraries:
- `yarn build:all && yarn publish:all`

## How to create new component

_TBD_

## How to test

_TBD_
