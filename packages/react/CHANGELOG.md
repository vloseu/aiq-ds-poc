# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.17](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.16...@aiq-ds/react@0.1.17) (2020-12-17)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.16](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.15...@aiq-ds/react@0.1.16) (2020-12-16)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.15](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.14...@aiq-ds/react@0.1.15) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.14](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.13...@aiq-ds/react@0.1.14) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.13](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.12...@aiq-ds/react@0.1.13) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.12](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.11...@aiq-ds/react@0.1.12) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.11](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.10...@aiq-ds/react@0.1.11) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.10](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.9...@aiq-ds/react@0.1.10) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.9](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.8...@aiq-ds/react@0.1.9) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.8](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.7...@aiq-ds/react@0.1.8) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.7](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.6...@aiq-ds/react@0.1.7) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.6](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.5...@aiq-ds/react@0.1.6) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.5](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.4...@aiq-ds/react@0.1.5) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.4](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.3...@aiq-ds/react@0.1.4) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.3](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.1...@aiq-ds/react@0.1.3) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.2](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.1...@aiq-ds/react@0.1.2) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





## [0.1.1](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/react@0.1.0...@aiq-ds/react@0.1.1) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/react





# 0.1.0 (2020-11-18)


### Features

* **monorepo:** init monorepo with test component ([47b30eb](https://bitbucket.org/vloseu/aiq-ds-poc/commits/47b30ebd7e81f43fbfe9c7a4b7a9f5d70af9ea94))
