# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.6](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.2.5...@aiq-ds/core@0.2.6) (2020-12-17)

**Note:** Version bump only for package @aiq-ds/core





## [0.2.5](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.2.4...@aiq-ds/core@0.2.5) (2020-12-16)

**Note:** Version bump only for package @aiq-ds/core





## [0.2.4](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.2.3...@aiq-ds/core@0.2.4) (2020-11-19)


### Bug Fixes

* **core:** updated my-component ([7064b43](https://bitbucket.org/vloseu/aiq-ds-poc/commits/7064b43178bcb5f9d3e9314c54ff7bf8ab15d573))





## [0.2.3](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.2.2...@aiq-ds/core@0.2.3) (2020-11-19)


### Bug Fixes

* **core:** fix props in my-component ([c163a08](https://bitbucket.org/vloseu/aiq-ds-poc/commits/c163a08104b1e64aef750192e9d7e5823793e18d))





## [0.2.2](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.2.1...@aiq-ds/core@0.2.2) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/core





## [0.2.1](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.2.0...@aiq-ds/core@0.2.1) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/core





# [0.2.0](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.11...@aiq-ds/core@0.2.0) (2020-11-19)


### Features

* **core:** update my-component ([100ceba](https://bitbucket.org/vloseu/aiq-ds-poc/commits/100ceba631de4c4f9b4d700e77488f4ce3cb4260))





## [0.1.11](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.10...@aiq-ds/core@0.1.11) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.10](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.8...@aiq-ds/core@0.1.10) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.5](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.8...@aiq-ds/core@0.1.5) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.8](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.7...@aiq-ds/core@0.1.8) (2020-11-19)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.7](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.6...@aiq-ds/core@0.1.7) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.6](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.5...@aiq-ds/core@0.1.6) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.5](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.4...@aiq-ds/core@0.1.5) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.4](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.3...@aiq-ds/core@0.1.4) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.3](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.1...@aiq-ds/core@0.1.3) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.2](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.1...@aiq-ds/core@0.1.2) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





## [0.1.1](https://bitbucket.org/vloseu/aiq-ds-poc/compare/@aiq-ds/core@0.1.0...@aiq-ds/core@0.1.1) (2020-11-18)

**Note:** Version bump only for package @aiq-ds/core





# 0.1.0 (2020-11-18)


### Features

* **monorepo:** init monorepo with test component ([47b30eb](https://bitbucket.org/vloseu/aiq-ds-poc/commits/47b30ebd7e81f43fbfe9c7a4b7a9f5d70af9ea94))
