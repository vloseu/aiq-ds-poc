import { Component, Prop, h } from '@stencil/core';
import { format } from '../../utils/utils';

export interface Address {
  city: string;
  country: string;
}

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: true,
})
export class MyComponent {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  @Prop() address: Address;

  private getAddress(): string {
    return `${this.address.city}, ${this.address.country}`;
  }

  private getFullName(): string {
    return format(this.first, this.middle, this.last);
  }

  render() {
    return <div>Hey there! I'm {this.getFullName()} from {this.getAddress()}</div>;
  }
}
