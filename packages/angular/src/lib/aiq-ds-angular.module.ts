import { NgModule } from '@angular/core';

import { MyComponent } from "./directives/proxies";

const PROXIES = [MyComponent];

@NgModule({
  declarations: PROXIES,
  exports: PROXIES
})
export class AiqDsAngularModule { }
