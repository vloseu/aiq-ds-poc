/*
 * Public API of @aiq-ds/angular
 */

export * from "./lib/directives/proxies";
export * from './lib/aiq-ds-angular.module';
